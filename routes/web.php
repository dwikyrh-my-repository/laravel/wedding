<?php

use App\Http\Controllers\DashboardController;
use App\Http\Controllers\InvitationController;
use App\Http\Controllers\LoginController;
use App\Http\Controllers\LogoutController;
use App\Http\Controllers\MessageController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::middleware(['auth'])->group(function () {
    # dashboard
    Route::get('/dashboard', DashboardController::class)->name('apps.dashboard.index');
    # messages
    Route::post('/messages', [MessageController::class, 'store'])->name('apps.message.store');
    # logout
    Route::post('/logout', LogoutController::class)->name('logout')->middleware(['auth']);
});

# invitations
Route::resource('/invitations', InvitationController::class, ['as' => 'apps']);

Route::middleware(['guest'])->group(function () {
    # login
    Route::get('/login', [LoginController::class, 'showLogin'])->name('login');
    Route::post('/login', [LoginController::class, 'loginUser'])->name('login');
});
