<?php

namespace Database\Seeders;

use App\Models\Bride;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;

class BrideTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Bride::create([
            'name' => 'Alfian Gilang Hidayat'
        ]);
        Bride::create([
            'name' => 'Anisa E Agustiani'
        ]);
    }
}
