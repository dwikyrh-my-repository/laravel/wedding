<?php

namespace Database\Seeders;

use App\Models\User;
use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Str;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'Alfian Gilang Hidayat',
            'email' =>  'alfian@mail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('alfiangh_77bdg'),
            'remember_token' => Str::random(10),
        ]);
        User::create([
            'name' => 'Anisa Eka Agustiani',
            'email' =>  'anisa@mail.com',
            'email_verified_at' => now(),
            'password' => bcrypt('anisa_77tgr'),
            'remember_token' => Str::random(10),
        ]);
    }
}
