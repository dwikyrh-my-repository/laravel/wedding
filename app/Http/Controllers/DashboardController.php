<?php

namespace App\Http\Controllers;

use App\Models\Invitation;
use Illuminate\Http\Request;

class DashboardController extends Controller
{
    # method invoke
    public function __invoke(Request $request)
    {
        # get all data invitations
        $invitations = Invitation::count();
        # get all data invtations by alfian gilang hidayat
        $invitations_alfian =  Invitation::whereRelation('bride', 'name', 'Alfian Gilang Hidayat')->count();
        $invitations_anisa =  Invitation::whereRelation('bride', 'name', 'Anisa E Agustiani')->count();
        # get all data invtations by anisa e agustiani
        return view('apps.dashboard.index', compact('invitations', 'invitations_alfian', 'invitations_anisa'));
    }
}
