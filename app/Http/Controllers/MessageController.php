<?php

namespace App\Http\Controllers;

use App\Models\Message;
use Illuminate\Http\Request;

class MessageController extends Controller
{
    # process create message
    public function store(Request $request)
    {
        # validation
        $request->validate([
            'invitation_id' => ['required'],
            'name' => ['required'],
            'body' => ['required'],
        ]);

        # action create message
        Message::create([
            'invitation_id' => $request->invitation_id,
            'name' => $request->name,
            'body' => $request->body,
        ]);

        # redirect
        return back()->with('success', 'Pesan berhasil dibuat.');
    }
}
