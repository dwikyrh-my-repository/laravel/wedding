<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Validation\ValidationException;

class LoginController extends Controller
{
    # view login
    public function showLogin()
    {
        # return view
        return view('apps.auth.login');
    }

    # action login 
    public function loginUser(Request $request)
    {
        # validation
        $request->validate([
            'email' => ['required', 'email'],
            'password' => ['required'],
        ]);

        # process login user
        if(!Auth::attempt($request->only('email', 'password'), $request->boolean('remember'))) {
            throw ValidationException::withMessages([
                'password' => 'These credentials do not match our records.'
            ]);
        }
        $request->session()->regenerate();

        # redirect
        return to_route('apps.dashboard.index');
    }
}
