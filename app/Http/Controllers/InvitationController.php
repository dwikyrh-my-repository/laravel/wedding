<?php

namespace App\Http\Controllers;

use App\Models\Bride;
use App\Models\Invitation;
use App\Models\Message;
use Illuminate\Http\Request;

class InvitationController extends Controller
{
    # __construct for middleware
    public function __construct()
    {
        $this->middleware(['auth'])->except(['show']);
    }

    # get all data invitations
    public function index(Invitation $invitation)
    {
        # get all data invitations
        $invitations = Invitation::query()->when(request()->q, function($invitations) {
            $invitations->where('name', 'like', '%' . request()->q . '%')
            ->orWhereRelation('bride', 'name', 'like', '%' . request()->q . '%');
        })->latest()->paginate(25);

        # return view
        return view('apps.invitations.index', compact('invitations', 'invitation'));
    }

    # view create invitation
    public function create(Invitation $invitation)
    {
        # get all data bride
        $brides = Bride::all();

        # return view
        return view('apps.invitations.create', compact('brides'));
    }

    # action create invitation
    public function store(Request $request)
    {
        # validation
        $request->validate([
            'name' => ['required'],
            'bride_id' => ['required'],
            'status' => ['required'],
        ]);

        # create invitation
        Invitation::create([
            'name' => $request->name,
            'slug' => str($request->name . ' ' . str()->random(7))->slug(),
            'bride_id' => $request->bride_id,
            'status' => $request->status,
        ]);

        # redirect
        return to_route('apps.invitations.index');
    }

    # view invited guest
    public function show(Invitation $invitation, Message $message)
    {
        # show message by guest
        $messages = $invitation->messages()->get();

        # return view
        return view('apps.invitations.show', compact('invitation', 'messages', 'message'));
    }

    # view edit invitation
    public function edit(Invitation $invitation)
    {
        # get all data bride
        $brides = Bride::all();

        # return view
        return view('apps.invitations.edit', compact('brides', 'invitation'));
    }

    # action update invitation
    public function update(Request $request, Invitation $invitation)
    {
        # validation
        $request->validate([
            'name' => ['required'],
            'bride_id' => ['required'],
            'status' => ['required'],
        ]);

        # update invitation
        $invitation->update([
            'name' => $request->name,
            'bride_id' => $request->bride_id,
            'status' => $request->status,
        ]);

        # redirect
        return to_route('apps.invitations.index');
    }

    # action delete invitation
    public function destroy(Invitation $invitation)
    {
        # delete invitation
        $invitation->delete();

        # redirect
        return back();
    }
}
