<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Bride extends Model
{
    use HasFactory;

    # guarded
    protected $guarded = [];

    # relation table
    public function invitations()
    {
        return $this->hasMany(Invitation::class);
    }
}
