<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Message extends Model
{
    use HasFactory;

    # guarded
    protected $guarded = [];

    # relation table
    public function invitation()
    {
        return $this->belongsTo(Invitation::class);
    }
}
