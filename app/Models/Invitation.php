<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Invitation extends Model
{
    use HasFactory;

    # guarded
    protected $guarded = [];

    # relation table
    public function bride()
    {
        return $this->belongsTo(Bride::class);
    }

    public function messages()
    {
        return $this->hasMany(Message::class);
    }

    # route key name
    public function getRouteKeyName()
    {
        return 'slug';
    }
}
