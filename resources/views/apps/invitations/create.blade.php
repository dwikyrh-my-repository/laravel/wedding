@extends('apps.layouts.app')
@section('content')
  <div class="col-12 mb-4">
    <div class="card adow h-100">
      <div class="card-body">
        <h4 class="fw-bold mb-4">Tambah Tamu Undangan</h4>
        {{-- form create invitation --}}
        <form action="{{ route('apps.invitations.store') }}" method="POST">
          @csrf
          {{-- name --}}
          <div class="mb-3">
            <label for="name" class="form-label">Nama Tamu Undangan</label>
            <input type="text" name="name" class="form-control" id="name">
          </div>
          {{-- invitation from --}}
          <div class="mb-3">
            <label for="bride_Id" class="form-label">Tamu Undangan Dari</label>
            <select name="bride_id" id="bride_Id" class="form-control" id="bride_id">
              @foreach ($brides as $bride)
                <option value="{{ $bride->id }}">{{ $bride->name }}</option>
              @endforeach
            </select>
          </div>
          {{-- status --}}
          <div class="mb-3">
            <label for="status" class="form-label">Status</label>
            <input type="text" name="status" class="form-control" id="status">
          </div>
          {{-- button submit --}}
          <button type="submit" class="btn btn-primary">Create</button>
        </form>
      </div>
    </div>
  </div>
@endsection
