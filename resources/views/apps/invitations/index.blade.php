@extends('apps.layouts.app')
@section('content')
  <div class="col-12 col-md-12 mb-4">
    <div class="card adow h-100 py-2">
      <div class="card-body">
        <div class="row no-gutters align-items-center">
          <div class="col-12 col-sm-4 col-md-4 mb-3 mb-sm-0">
            <a href="{{ route('apps.invitations.create') }}" class="btn btn-primary">Tambah Tamu </a>
          </div>
          <div class="col-12 col-sm-8 col-xl-8">
            <form action="{{ route('apps.invitations.index') }}">
              <div class="input-group">
                <input type="search" class="form-control" name="q" placeholder="Cari Tamu Undangan">
                <div class="input-group-append">
                  <button type="submit" class="btn btn-primary">
                    <i class="fa fa-search"></i>
                  </button>
                </div>
              </div>
            </form>
          </div>
          <div class="table-responsive mt-2">
            <table class="table table-hovered">
              <thead>
                <tr>
                  <th>#</th>
                  <th>Nama Tamu Undangan</th>
                  <th>Tamu Undangan Dari</th>
                  <th>Status</th>
                  <th>Actions</th>
                </tr>
              </thead>
              <tbody>
                @forelse ($invitations as $invitation)
                  <tr>
                    <td>{{ $invitations->firstItem() + $loop->index }}</td>
                    <td>
                      <a href="{{ route('apps.invitations.show', $invitation) }}" target="_blank">
                        {{ $invitation->name }}
                      </a>
                    </td>
                    <td>{{ $invitation->bride->name }}</td>
                    <td><span class="badge bg-info text-white">{{ $invitation->status }}</span></td>
                    <td>
                      <a href="{{ route('apps.invitations.edit', $invitation) }}"
                        class="btn btn-sm btn-warning mb-2 mb-md-0">
                        <i class="fa fa-pencil-alt"></i>
                      </a>
                      <button onclick="Delete(this.id)" class="btn btn-sm btn-danger" id="{{ $invitation->slug }}">
                        <i class="fa fa-trash"></i>
                        </butt>
                    </td>
                  </tr>
                @empty
                  <tr>
                    <td colspan="4" class="text-center text-danger">Tidak ada data tamu</td>
                  </tr>
                @endforelse
              </tbody>
            </table>
          </div>
        </div>
        {{-- pagination --}}
        {{ $invitations->links() }}
      </div>
    </div>
  </div>

  <script>
    // ajax delete
    function Delete(id) {
      var id = id;
      var token = $("meta[name='csrf-token']").attr("content");

      Swal.fire({
        title: "Ingin menghapus data ini?",
        text: "Data yang sudah dihapus tidak dapat dikembalikan lagi!",
        icon: "warning",
        showCancelButton: true,
        confirmButtonColor: '#3085d6',
        cancelButtonColor: '#d33',
        confirmButtonText: 'Yes, delete it!'
      }).then((result) => {
        if (result.isConfirmed) {
          jQuery.ajax({
            url: "/invitations/" + id,
            data: {
              "id": id,
              "_token": token
            },
            type: "DELETE"
          });
          Swal.fire({
            position: 'center',
            icon: 'success',
            title: 'Data tamu berhasil di hapus',
            showConfirmButton: false,
            timer: 1500,
          }).then(function() {
            location.reload();
          });
        }
      });
    }
  </script>
@endsection
