<!DOCTYPE html>
<html lang="en">

<head>
  @include('apps.invitations.partials.show.head')
</head>

<body>
  <!-- audio backsound -->
  {{-- <audio src="{{ asset('assets/audio/backsound.mp3') }}" id="myaudio" class="d-none" preload="auto" controls loop autoplay></audio> --}}

  {{-- wedding modal --}}
  @include('apps.invitations.partials.show.wedding-modal')

  {{-- hero --}}
  @include('apps.invitations.partials.show.hero')

  <main id="main">
    {{-- bride --}}
    @include('apps.invitations.partials.show.bride')

    {{-- ceremony reception --}}
    @include('apps.invitations.partials.show.ceremony-reception')

    {{-- reception location  --}}
    @include('apps.invitations.partials.show.reception-location')

    {{-- memoreable moments --}}
    @include('apps.invitations.partials.show.memoreable')

    {{-- count down & message --}}
    @include('apps.invitations.partials.show.count-down-message')

    {{-- amplop --}}
    @include('apps.invitations.partials.show.amplop')
    
    {{-- prayer --}}
    @include('apps.invitations.partials.show.prayer')

    {{-- closing --}}
    @include('apps.invitations.partials.show.closing')
  </main>

  {{-- footer --}}
  @include('apps.invitations.partials.show.footer')
  {{-- javascript --}}
  @include('apps.invitations.partials.show.js')

  {{-- javascript stack --}}
  @stack('scripts')
</body>

</html>
