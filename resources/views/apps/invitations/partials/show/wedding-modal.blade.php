<div class="container">
  <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-fullscreen">
      <div class="modal-content">
        <div class="modal-body">
          <div class="row d-flex align-items-center justify-content-center">
            <div class="col-12 col-md-8">
              <div class="card-wedding">
                <div class="wedding-text text-white text-center p-5">
                  <h3 class="bismillah">ِبِسْمِ اللَّهِ الرَّحْمَنِ الرَّحِيْم</h3>
                  <h3 class="invitation">UNDANGAN PERNIKAHAN</h3>
                  <h3>MINGGU, 28 MEI 2023</h3>
                  <h3 class="couple">ANISA <span class="d-block d-md-inline">&</span> ALFIAN</h3>
                  <hr style="border: 5px solid #fff;">

                  <h3>SPECIAL INVITE TO :</h3>
                  <h3 class="py-2 fs-3 guest text-uppercase">{{ $invitation->name }}</h3>
                  <h3 class="mb-4">DI TEMPAT.</h3>
                  <button type="button" class="btn btn-sm btn-md-lg text-white fw-bold" data-bs-dismiss="modal">
                    <svg xmlns="http://www.w3.org/2000/svg" width="27" height="27" fill="currentColor"
                      class="bi bi-envelope me-2" viewBox="0 0 16 16">
                      <path
                        d="M0 4a2 2 0 0 1 2-2h12a2 2 0 0 1 2 2v8a2 2 0 0 1-2 2H2a2 2 0 0 1-2-2V4Zm2-1a1 1 0 0 0-1 1v.217l7 4.2 7-4.2V4a1 1 0 0 0-1-1H2Zm13 2.383-4.708 2.825L15 11.105V5.383Zm-.034 6.876-5.64-3.471L8 9.583l-1.326-.795-5.64 3.47A1 1 0 0 0 2 13h12a1 1 0 0 0 .966-.741ZM1 11.105l4.708-2.897L1 5.383v5.722Z" />
                    </svg>
                    BUKA UNDANGAN
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</div>

<script>
  var i = 0;
  var j = 0;

  setTimeout(() => {

  }, 2000);

  setTimeout(() => {
    function mengetik_two() {
      if (j < text_two.length) {
        document.getElementById('bride-title-two').innerHTML += text_two.charAt(j);
        j++;
        setTimeout(mengetik_two, 65);
      }
    }
    mengetik_two();
  }, 7500)
</script>
