<section id="hero">
  <div class="hero-container">
    <div id="heroCarousel" class="carousel slide carousel-fade" data-bs-ride="carousel" data-bs-interval="5000">
      <div class="carousel-inner" role="listbox">
        <div class="carousel-item active">
          <div class="carousel-container">
            <div class="carousel-content">
              <h5 class="mb-4">The Wedding Of</h5>
              <h2> ANISA & ALFIAN</h2>
              <h5>Sunday - 28 May 2023</h5>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>