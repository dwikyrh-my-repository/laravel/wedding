<section class="count-down-message">
  <div class="container">
    <div class="row d-flex justify-content-center" data-aos="fade-up" data-aos-duration="2000">
      <div class="col-md-8">
        <div class="card shadow border-0 p-4">
          <div class="section-title">
            <h2> Wedding Starts In</h2>
          </div>
          <!-- reception countdown -->
          <div class="card-body d-flex justify-content-center">
            <div class="row">
              <div class="col-3">
                <h6 id="days" class="fw-bold lead text-center rounded p-2"></h6>
              </div>
              <div class="col-3">
                <h6 id="hours" class="fw-bold lead text-center rounded p-2"></h6>
              </div>
              <div class="col-3">
                <h6 id="minutes" class="fw-bold lead text-center rounded p-2"></h6>
              </div>
              <div class="col-3">
                <h6 id="seconds" class="fw-bold lead text-center rounded p-2"></h6>
              </div>
            </div>
          </div>

          {{-- message --}}
          <div class="row justify-content-center">
            <div class="section-title">
              <div class="col-12">
                <h2 class="text-center"
                  style="font-family: 'Medula One', cursive; font-size: 2em; letter-spacing: 5px; color: #F3CB51;">
                  Ucapan & Reservasi </h2>
              </div>
            </div>
            <div class="col-12">
              <h4 class="text-center mb-4"
                style="font-family: 'Medula One', cursive; font-size: 1.7em; letter-spacing: 2x; margin-top: -30px;">
                KIRIMKAN PESAN UNTUK KEDUA CALON PENGANTIN
              </h4>
            </div>
            <div class="col-12 col-md-8">
              {{-- form message --}}
              <form action="{{ route('apps.message.store') }}" class="p-2" method="post">
                @csrf
                <input type="hidden" name="invitation_id" class="form-control" value="{{ $invitation->id }}" readonly>

                {{-- name --}}
                <div class="mb-3">
                  <label for="name" class="form-label">Nama</label>
                  <input type="text" name="name" class="form-control" id="name" value="{{ $invitation->name }}" readonly>
                </div>

                {{-- body --}}
                <div class="mb-3">
                  <label for="body" class="form-label">Pesan</label>
                  <textarea name="body" id="" cols="30" rows="7" class="form-control" placeholder="Pesan"></textarea>
                </div>

                {{-- rsvp --}}
                <div class="mb-3">
                  <label for="confirmation" class="form-labe"></label>
                </div>
                
                {{-- button submit --}}
                <div class="mb-3">
                  <button type="submit" class="btn text-white" style="background-color: #F3CB51;">
                    Kirim
                  </button>
                </div>
            </div>
            </form> 
            
            <div class="col-12 col-md-8">
              {{-- guest message --}}
              @if ($messages->count() > 0)
                <div class="card shadow rounded border-0 mb-5">
                  <div class="card-body">
                    <div class="message">
                      @foreach ($messages as $message)
                        <div class="text-center message-body">
                          " {{ $message->body }} "
                        </div>
                      @endforeach
                      <div class="fw-bold fs-2 text-center mt-2" style="font-family: 'Niconne', cursive;">
                        {!! $message->name !!}
                      </div>
                    </div>
                  </div>
                </div>
              @endif
            </div>
          </div>

        </div>
      </div>
    </div>
  </div>
  </div>
</section>
