<section class="ceremony-reception rounded">
  <div class="container">
    {{-- akad --}}
    <div class="akad" data-aos="fade-up" data-aos-duration="2000">
      <div class="row mb-3">
        <div class="col-md-12 text-center text-white">
          <h1 class="fw-bold" style="font-size: 4em; font-family: 'Medula One', cursive; letter-spacing: 8px;">
            AKAD NIKAH
          </h1>
        </div>
      </div>
      <div class="row d-flex justify-content-center text-white text-center">
        <div class="col-md-4 mb-3">
          <h2 class="fw-bold">
            <div>MINGGU</div>
            08:00 WIB
          </h2>
        </div>
        <div class="col-md-4 rounded-circle mb-3">
          <h2 class="fw-bold" style="font-size: 75px;">28</h2>
        </div>
        <div class="col-md-4 mb-3">
          <h2 class="fw-bold">
            MEI <br />
            2023
          </h2>
        </div>
      </div>
    </div>

    <div class="row" data-aos="zoom-in" data-aos-duration="2000">
      <div class="col-md-12 text-center text-white">
      </div>
    </div>
    <hr style="color: white; border: 20px solid #fff; width: 100%;">

    {{-- resepsi --}}
    <div class="reception" data-aos="fade-up" data-aos-duration="2000">
      <div class="row mb-3 mt-5">
        <div class="col-md-12 text-center text-white">
          <h1 class="fw-bold" style="font-size: 4em; font-family: 'Medula One', cursive; letter-spacing: 8px;">
            RESEPSI NIKAH
          </h1>
        </div>
      </div>
      <div class="row d-flex justify-content-center text-white text-center">
        <div class="col-md-4 mb-3">
          <h2 class="fw-bold">
            <div>MINGGU</div>
            11:00 - 14:00 WIB
          </h2>
        </div>
        <div class="col-md-4 rounded-circle mb-3">
          <h2 class="fw-bold" style="font-size: 75px;">28</h2>
        </div>
        <div class="col-md-4 mb-3">
          <h2 class="fw-bold">
            MEI <br />
            2023
          </h2>
        </div>
      </div>

    </div>
    <div class="row" data-aos="zoom-in" data-aos-duration="2000">
      <div class="col-md-12 text-center text-white">
      </div>
    </div>
  </div>
</section>
