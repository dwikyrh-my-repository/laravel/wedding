<section class="prayer rounded">
  <div class="container">
    <p data-aos="zoom-in" data-aos-duration="2000">
      “Semoga Allah menghimpun yang terserak dari keduanya, memberkati mereka berdua dan kiranya Allah
      meningkatkan kualitas keturunan mereka,
      menjadikannya pembuka pintu rahmat, sumber ilmu dan hikmah serta pemberi rasa aman bagi umat.”
    </p>
    <p class="mt-5" data-aos="zoom-in" data-aos-duration="2000">
      (Doa Nabi Muhammad SAW, pada pernikahan putrinya Fatimah Azzahra dengan Ali Bin Abi Thalib)
    </p>
  </div>
</section>