    <!-- Vendor JS Files -->
    <script src="{{ asset('vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

    <!-- aos -->
    <script src="https://unpkg.com/aos@2.3.1/dist/aos.js"></script>
    <!-- Template Main JS File -->
    <script src="{{ asset('assets/js/main.js') }}"></script>
    <script src="{{ asset('assets/js/jquery-3.6.3.min.js') }}"></script>

    <!-- swipper -->
    <script src="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.js"></script>
    <script>
      var swiper = new Swiper(".mySwiper", {
        loop: true,
        pagination: {
          el: ".swiper-pagination",
          clickable: true,
        },
        navigation: {
          nextEl: ".swiper-button-next",
          prevEl: ".swiper-button-prev",
        },
        breakpoints: {
          640: {
            slidesPerView: 1,
            spaceBetween: 20,
          },
          768: {
            slidesPerView: 2,
            spaceBetween: 40,
          },
          1024: {
            slidesPerView: 3,
            spaceBetween: 50,
          },
        },
      });
    </script>

    <!-- count down -->
    <script>
      // Set the date we're counting down to
      var countDownDate = new Date("May 28, 2023 00:00:00").getTime();

      // Update the count down every 1 second
      var x = setInterval(function() {

        // Get today's date and time
        var now = new Date().getTime();

        // Find the distance between now and the count down date
        var distance = countDownDate - now;

        // Time calculations for days, hours, minutes and seconds
        var days = Math.floor(distance / (1000 * 60 * 60 * 24));
        var hours = Math.floor((distance % (1000 * 60 * 60 * 24)) / (1000 * 60 * 60));
        var minutes = Math.floor((distance % (1000 * 60 * 60)) / (1000 * 60));
        var seconds = Math.floor((distance % (1000 * 60)) / 1000);

        document.getElementById("days").innerHTML = days + ' D';
        document.getElementById("hours").innerHTML = hours + ' H';
        document.getElementById("minutes").innerHTML = minutes + ' M';
        document.getElementById("seconds").innerHTML = seconds + ' S';

        // If the count down is finished, write some text
        if (distance < 0) {
          clearInterval(x);
          document.getElementById("days").innerHTML = "0 D";
          document.getElementById("hours").innerHTML = "0 H";
          document.getElementById("minutes").innerHTML = "0 M";
          document.getElementById("seconds").innerHTML = "0 S";
          document.getElementById("demo").innerHTML = "ACARA SEDANG BERLANGSUNG";
        }
      }, 1000);
    </script>

    <!-- volume backsound -->
    {{-- <script>
        let audio = document.getElementById("myaudio");
    audio.volume = 0.1;
    </script> --}}

    <!-- modal -->
    <script>
      $(window).on('load', function() {
        $('#exampleModal').modal('show');
      });
    </script>

    <!-- aos -->
    <script>
      AOS.init();
    </script>

{{-- toast --}}
<script type="text/javascript" src="https://cdn.jsdelivr.net/npm/toastify-js"></script>
<script>
  @if(session('success'))
    Toastify({
    text: "{{ session('success') }}",
    duration: 3000,
    newWindow: true,
    close: false,
    gravity: "top", // `top` or `bottom`
    position: "center", // `left`, `center` or `right`
    stopOnFocus: true, // Prevents dismissing of toast on hover
    style: {
      background: "linear-gradient(to right, #00b09b, #96c93d)",
    },
    onClick: function(){} // Callback after click
  }).showToast();
  @endif
</scrip>