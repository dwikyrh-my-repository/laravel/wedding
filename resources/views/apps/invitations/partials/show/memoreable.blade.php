<section id="memoreable">
<div class="container">
  <div class="section-title">
    <h2 style="font-family: 'Medula One', cursive; font-size: 2.5em; letter-spacing: 5px;" data-aos="fade-up"
      data-aos-duration="2000">
      The Memorable Moments
    </h2>
  </div>

  <div class="swiper mySwiper" data-aos="zoom-in" data-aos-duration="2000">
    <div class="swiper-wrapper">
      <div class="swiper-slide">
        <div class="col-lg-12 col-md-12">
          <div class="memoreable-moments-foto">
            <img src="{{ asset('assets/img/memoreable-one.png') }}" class="img-fluid rounded" />
          </div>
        </div>
      </div>
      <div class="swiper-slide">
        <div class="col-lg-12 col-md-12">
          <div>
            <img src="{{ asset('assets/img/memoreable-one.png') }}" class="img-fluid rounded"
              style="height: 400px; width: 100%;">
          </div>
        </div>
      </div>
      <div class="swiper-slide">
        <div class="col-lg-12 col-md-12">
          <div>
            <img src="{{ asset('assets/img/memoreable-one.png') }}" class="img-fluid rounded"
              style="height: 400px; width: 100%;">
          </div>
        </div>
      </div>
      <div class="swiper-slide">
        <div class="col-lg-12 col-md-12">
          <div>
            <img src="{{ asset('assets/img/memoreable-one.png') }}" class="img-fluid rounded"
              style="height: 400px; width: 100%;">
          </div>
        </div>
      </div>
      <div class="swiper-slide">
        <div class="col-lg-12 col-md-12">
          <div>
            <img src="{{ asset('assets/img/memoreable-one.png') }}" class="img-fluid rounded"
              style="height: 400px; width: 100%;">
          </div>
        </div>
      </div>
    </div>
    <div class="swiper-button-next"></div>
    <div class="swiper-button-prev"></div>
    <div class="swiper-pagination"></div>
  </div>
</div>
</section>

