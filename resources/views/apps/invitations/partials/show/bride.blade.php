<section id="bride" class="bride">
  <div class="container">
    <div class="bride-title" data-aos="fade-up" data-aos-duration="2000">
      " Dan di antara tanda-tanda (kebesaran)-Nya ialah Dia menciptakan pasangan-pasangan untukmu dari jenismu sendiri,
      agar kamu cenderung dan merasa tenteram kepadanya, dan Dia menjadikan di antaramu rasa kasih dan sayang. "
      <div class="mt-1 mt-md-0"></div>
      (QS. Ar-Rum Ayat 21)
    </div>
    <div class="bride-title mt-4" data-aos="fade-up" data-aos-duration="2000">
      Assalamu'alaikum warahmatullahi wabarakatuh Dengan memohon rahmat dan ridho Allah SWT, Mohon do'a restu Bapak/Ibu/Saudara dalam rangka melangsungkan pernikahan putra putri kami :
    </div>
    <div class="bride-content">
      <div class="row mt-5 justify-content-center">
        {{-- anisa --}}
        <div class="col-md-5 col-lg-4" data-aos="fade-right" data-aos-duration="2000">
          <div class="card shadow border-0 p-2">
            <div class="card-body d-flex justify-content-center align-items-center">
              <img src="{{ asset('assets/img/anisa-1.png') }}" alt="" class="img-fluid"
                style="width: 65% !important; height: 45%;" />
              <br>
            </div>
            <h2 class="text-center">
              Anisa Eka Agustiani
            </h2>
            <h5 class="text-center">
              Putri Pertama
            </h5>
            <h5 class="text-center">
              Bapak Iwan Rusmawan <br>
              Ibu Sartinah
            </h5>
          </div>
        </div>

        <div class="col-md-2 col-lg-2 d-flex align-items-center justify-content-center mt-4 mb-4">
          <h2 style="font-size: 110px; color: #A68303;" class="text-center" data-aos="zoom-in" data-aos-duration="2000">
            &
          </h2>
        </div>

        {{-- alfian --}}
        <div class="col-md-5 col-lg-4" data-aos="fade-left" data-aos-duration="2000">
          <div class="card shadow border-0 p-2">
            <div class="card-body d-flex justify-content-center align-items-center">
              <img src="{{ asset('assets/img/alfian-1.png') }}" alt="" class="img-fluid"
                style="width: 65% !important; height: 45%;" /> <br>
            </div>
            <h2 class="text-center">
              Alfian Gilang Hidayat
            </h2>
            <h5 class="text-center">
              Putra Pertama
            </h5>
            <h5 class="text-center">
              Bapak Asep Hidayat Hedriyanto <br>
              Ibu Aminah Herawati
            </h5>
          </div>
        </div>

      </div>
    </div>
  </div>
</section>

<script>
  var i = 0;
  var j = 0;

  setTimeout(() => {
    document.getElementById('bride-surah-ar-rum-21').innerHTML = '(QS. Ar-Rum Ayat 21)'
  }, 2000);

  setTimeout(() => {
    function mengetik_two() {
      if (j < text_two.length) {
        document.getElementById('bride-title-two').innerHTML += text_two.charAt(j);
        j++;
        setTimeout(mengetik_two, 65);
      }
    }
    mengetik_two();
  }, 7500)
</script>
