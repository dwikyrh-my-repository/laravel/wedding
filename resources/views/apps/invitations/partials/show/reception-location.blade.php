<section class="reception-location">
  <div class="card shadow border-0 p-2">
    <div class="card-body">
      <div class="container mb-5">
        <div class="section-title">
          <h2 style="font-family: 'Medula One', cursive; font-size: 2.5em; letter-spacing: 5px;" data-aos="fade-up"
            data-aos-duration="2000">RECEPTION LOCATION</h2>
        </div>
        <div class="row">
          <div class="col-md-12 d-flex justify-content-center">
            <iframe style="border: 10px solid #A68303; border-radius: 10px;" data-aos="fade-up" data-aos-duration="2000"
              src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3965.361248271685!2d106.6791544!3d-6.347246999999999!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x2e69e5965ba53e4f%3A0xa6442b7bb4699f33!2sTeras%20Tangsel!5e0!3m2!1sen!2sid!4v1674988173712!5m2!1sen!2sid"
              width="1000" height="450" style="border:0;" allowfullscreen="" loading="lazy"
              referrerpolicy="no-referrer-when-downgrade"></iframe>
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
