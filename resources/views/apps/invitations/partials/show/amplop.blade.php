<section class="amplop">
  <div class="container">
    <div class="row d-flex justify-content-center" data-aos="fade-up" data-aos-duration="2000">
      <div class="col-md-6">
        <div class="card shadow border-0 p-2">
          <div class="section-title">
            <h2 style="font-family: 'Medula One', cursive; font-size: 2.5em; letter-spacing: 5px;" class="mt-3">
              Amplop Digital
            </h2>
          </div>

          {{-- message --}}
          <div class="row d-flex justify-content-center">
            <div class="section-title">
              <h4 class="text-center p-2 amplop-title">
                Doa Restu Anda merupakan karunia yang sangat berarti bagi kami.
                <div class="mb-2"></div>
                Dan jika memberi adalah ungkapan tanda kasih Anda, Anda dapat memberi kado secara cashless.
              </h4>
            </div>
            <div class="col-12 col-md-10 mb-5">
              <div class="accordion" id="accordionFlushExample">
                {{-- bca --}}
                <div class="accordion-item">
                  <h2 class="accordion-header" id="flush-headingOne">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                      data-bs-target="#flush-collapseOne" aria-expanded="false" aria-controls="flush-collapseOne">
                      <img src="{{ asset('assets/img/bca.jpg') }}" style="width: 70px; ">
                    </button>
                  </h2>
                  <div id="flush-collapseOne" class="accordion-collapse collapse justify-content-center" aria-labelledby="flush-headingOne"
                    data-bs-parent="#accordionFlushExample">
                    <div class="accordion-body text-center">
                      <img src="{{ asset('assets/img/ovo-barcode-alfian.jpg') }}" alt="" style="width: 200px"
                        class="mb-3">
                      <h6><strong>08123456789</strong></h6>
                      <h6>Atas Nama: <strong>Alfian Gilang Hidayat</strong></h6>
                    </div>
                  </div>
                </div>
                {{-- mandiri --}}
                <div class="accordion-item">
                  <h2 class="accordion-header" id="flush-headingTwo">
                    <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                      data-bs-target="#flush-collapseTwo" aria-expanded="false" aria-controls="flush-collapseTwo">
                      <img src="{{ asset('assets/img/mandiri.jpg') }}" style="width: 95px; ">
                    </button>
                  </h2>
                  <div id="flush-collapseTwo" class="accordion-collapse collapse justify-content-center" aria-labelledby="flush-headingTwo"
                    data-bs-parent="#accordionFlushExample">
                    <div class="accordion-body text-center">
                      <img src="{{ asset('assets/img/ovo-barcode-alfian.jpg') }}" alt="" style="width: 200px"
                        class="mb-3">
                      <h6><strong>08123456789</strong></h6>
                      <h6>Atas Nama: <strong>Alfian Gilang Hidayat</strong></h6>
                    </div>
                  </div>
                </div>
              </div>
            </div>

          </div>
        </div>
      </div>
</section>
