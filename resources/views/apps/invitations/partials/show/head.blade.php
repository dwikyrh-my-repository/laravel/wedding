<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1.0" name="viewport">
<title>Wedding Anisa & Alfian - {{ $invitation->name }} Invitation </title>

{{-- favicon --}}
<link href="{{ asset('assets/img/favicon.jpg') }}" rel="icon">

{{-- google fonts --}}
<link rel="preconnect" href="https://fonts.googleapis.com">
<link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
<link href="https://fonts.googleapis.com/css2?family=Inter&family=Medula+One&family=Niconne&display=swap"
  rel="stylesheet">

{{-- vendor css file --}}
<link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

{{-- swiper --}}
<link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.css" />

{{-- memoreable --}}
{{-- <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/uikit@3.15.24/dist/css/uikit.min.css" /> --}}

{{-- aos --}}
<link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
{{-- template main file css --}}
<link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">

{{-- toast js --}}
<link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css">