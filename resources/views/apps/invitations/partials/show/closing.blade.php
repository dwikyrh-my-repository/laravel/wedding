<section class="closing">
  <div class="container">
    <div class="row d-flex justify-content-center">
      <div class="col-md-8">
        <div class="card shadow border-0 p-5" 
        style="bac">
          <h4 style="font-family: 'Medula One', sans-serif; font-size: 1.4em; text-align: center; color: #3d3d3d;"
            data-aos="zoom-in" data-aos-duration="2000">
            Merupakan suatu kehormatan dan kebahagiaan bagi kami sekeluarga apabila
            Bapak/Ibu/Saudara/i berkenan hadir untuk memberikan doa restu kepada kedua mempelai.
            Atas kehadiran serta doa restu, kami ucapkan terima kasih.
          </h4>
          <div class="prayer-closing">
            <h6 class="text-center" data-aos="zoom-in" data-aos-duration="2000">
              Turut berbahagia Segenap keluarga besar
            </h6>
          </div>

          <div class="couple-closing mt-3">
            <img src="{{ asset('assets/img/flower.jpg') }}" data-aos="zoom-in" data-aos-duration="2000">
            <h6 class="text-center" data-aos="zoom-in" data-aos-duration="2000">
              Anisa <span class="d-block d-md-inline">&</span> Alfian
            </h6>
            <img src="{{ asset('assets/img/flower-two.jpg') }}" data-aos="zoom-in" data-aos-duration="2000">
          </div>
        </div>
      </div>
    </div>
  </div>
</section>
