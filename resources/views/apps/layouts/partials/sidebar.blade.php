<ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

    {{-- sidebar brand --}}
    <a class="sidebar-brand d-flex align-items-center justify-content-center"
        href="{{ route('apps.dashboard.index') }}">
        <div class="sidebar-brand-icon rotate-n-15">
            <i class="fas fa-laugh-wink"></i>
        </div>
        <div class="sidebar-brand-text mx-3">Wedding</div>
    </a>

    {{-- divider --}}
    <hr class="sidebar-divider my-0">

    {{-- nav item - dashboard --}}
    <li class="nav-item {{ request()->routeIs('apps.dashboard.index') ? ' active' :  '' }}">
        <a class="nav-link" href="{{ route('apps.dashboard.index') }}">
            <i class="fas fa-fw fa-tachometer-alt"></i>
            <span>Dashboard</span>
        </a>
    </li>

    {{--  nav item - invitations --}}
    <li class="nav-item {{ request()->routeIs('apps.invitations*') ? ' active' :  '' }}">
        <a class="nav-link" 
            href="{{ route('apps.invitations.index') }}">
            <i class="fas fa-users fa-table"></i>
            <span>Tamu Undangan</span>
        </a>
    </li>

    {{-- sidebar toggler - sidebar --}}
    <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
    </div>
</ul>