<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
  <meta name="description" content="">
  <meta name="author" content="">

  {{-- favicon --}}
  <link rel="shortcut icon" href="{{ asset('assets/img/favicon.jpg') }}" type="image/x-icon">
  {{-- csrf token --}}
  <meta name="csrf-token" content="{{ csrf_token() }}">
  {{-- custom fonts for this template --}}
  <link href="{{ asset('assets/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link
    href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
    rel="stylesheet">
  {{-- custom styles for this template --}}
  <link href="{{ asset('assets/css/sb-admin-2.min.css') }}" rel="stylesheet">

  {{-- sweetalert2 --}}
  <script src="//cdn.jsdelivr.net/npm/sweetalert2@11"></script>

  @stack('styles')
  <title>Wedding Anisa & Alfian</title>
</head>

<body id="page-top">

  {{-- page wrapper --}}
  <div id="wrapper">

    {{-- sidebar --}}
    @include('apps.layouts.partials.sidebar')

    {{-- content wrapper --}}
    <div id="content-wrapper" class="d-flex flex-column">

      {{-- main content --}}
      <div id="content">

        @include('apps.layouts.partials.top-bar')
        {{-- begin page content --}}
        <div class="container-fluid">
          {{-- content row --}}
          <div class="row">
            @yield('content')
          </div>
        </div>
      </div>

      {{-- footer --}}
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span><strong>&copy;Anisa & Alfian.</strong> All Rights Reserved</span>
          </div>
        </div>
      </footer>
    </div>
  </div>

  {{-- scroll to top bottom --}}
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  {{-- bootstrap core javascript --}}
  <script src="{{ asset('assets/vendor/jquery/jquery.min.js') }}"></script>
  <script src="{{ asset('assets/vendor/bootstrap/js/bootstrap.bundle.min.js') }}"></script>

  {{-- core plugin javascript --}}
  <script src="{{ asset('assets/vendor/jquery-easing/jquery.easing.min.js') }}"></script>

  {{-- custom scripts for all pages --}}
  <script src="{{ asset('assets/js/sb-admin-2.min.js') }}"></script>
</body>

</html>
