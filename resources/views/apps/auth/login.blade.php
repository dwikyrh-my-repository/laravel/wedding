<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

  <title>Anisa & Alfian Wedding - Login</title>

  <!-- Custom fonts for this template-->
  <link href="{{ asset('assets/vendor/fontawesome-free/css/all.min.css') }}" rel="stylesheet" type="text/css">
  <link
    href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i"
    rel="stylesheet">

  <!-- Custom styles for this template-->
  <link href="{{ asset('assets/css/sb-admin-2.min.css') }}" rel="stylesheet">

</head>

<body class="bg-gradient-primary">
  <div class="container">>
    <div class="row justify-content-center mt-5">
      <div class="col-12 col-md-6">
        <div class="card o-hidden border-0 shadow-lg my-5">
          <div class="card-body p-0">
            <div class="row">
              <div class="col-12">
                <div class="p-3 p-md-5">
                  <h4 class="text-center fw-bold">Login</h4>
                  {{-- form login --}}
                  <form action="{{ route('login') }}" method="POST" autocomplete="off" class="px-4 px-lg-2">
                    @csrf

                    {{-- email address --}}
                    <div class="mb-4 mt-4">
                      <input type="text" name="email" id="email"
                        class="form-control @error('email') is-invalid @enderror" value="{{ old('email') }}"
                        placeholder="Email Address">
                      @error('email')
                        <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>

                    {{-- password --}}
                    <div class="mb-4">
                      <input type="password" name="password" id="password"
                        class="form-control
                                      @error('password') is-invalid @enderror"
                        value="{{ old('password') }}" placeholder="Password">
                      @error('password')
                        <div class="invalid-feedback">{{ $message }}</div>
                      @enderror
                    </div>

                    {{-- button submit --}}
                    <div class="mb-4">
                      <button type="submit" class="btn btn-primary btn-md w-100 mt-2 mt-sm-0">Login</button>
                    </div>
                  </form>

                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>

</html>
