<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="utf-8">
  <meta content="width=device-width, initial-scale=1.0" name="viewport">
  <title>Anisa & Alfian Wedding</title>

  {{-- favicon --}}
  <link href="assets/img/favicon.png" rel="icon">
  <link href="assets/img/apple-touch-icon.png" rel="apple-touch-icon">

  {{-- google fonts --}}
  <link rel="preconnect" href="https://fonts.googleapis.com">
  <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
  <link href="https://fonts.googleapis.com/css2?family=Inter&family=Medula+One&family=Niconne&display=swap"
    rel="stylesheet">

  {{-- vendor css file --}}
  <link href="{{ asset('vendor/bootstrap/css/bootstrap.min.css') }}" rel="stylesheet">

  {{-- swiper --}}
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/swiper@8/swiper-bundle.min.css" />

  {{-- aos --}}
  <link href="https://unpkg.com/aos@2.3.1/dist/aos.css" rel="stylesheet">
  {{-- template main file css --}}
  <link href="{{ asset('assets/css/style.css') }}" rel="stylesheet">

  {{-- toast js --}}
  <link rel="stylesheet" type="text/css" href="https://cdn.jsdelivr.net/npm/toastify-js/src/toastify.min.css">
</head>

<body>
  <section id="hero" style="height: 100vh">
    <div class="hero-container">
      <div id="heroCarousel" class="carousel slide carousel-fade" data-bs-ride="carousel" data-bs-interval="5000">
        <div class="carousel-inner" role="listbox">
          <div class="carousel-item active" style="height: 100vh">
            <div class="carousel-container">
              <div class="carousel-content">
                <h5 class="mb-4">The Wedding Of</h5>
                <h2> ALFIAN & ANISA</h2>
                <h5>Sunday - 28 May 2023</h5>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
</body>

</html>
